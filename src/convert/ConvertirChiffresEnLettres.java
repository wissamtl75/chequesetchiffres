package convert;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class ConvertirChiffresEnLettres {
	
	   private static String montantFinale = "";
	
	  private static final String[] tensNames = {
	    "",
	    " dix",
	    " vingt",
	    " trente",
	    " quarante",
	    " cinquante",
	    " soixante",
	    " soixante-dix",
	    " quatre-vingt",
	    " quatre-vingt-dix"
	  };

	  private static final String[] numNames = {
	    "",
	    " un",
	    " deux",
	    " trois",
	    " quatre",
	    " cinq",
	    " six",
	    " sept",
	    " huit",
	    " neuf",
	    " dix",
	    " onze",
	    " douze",
	    " treize",
	    " quatorze",
	    " quinze",
	    " seize",
	    " dix-sept",
	    " dix-huit",
	    " dix-neuf"
	  };
	  

	  private static String convertirMoinsDeMille(int number) {
		  
	    String montant;
	    
	    if (number >= 10 && number < 100) {
	    	montantFinale = Integer.toString(number).substring(0,2);
	    }if (number >= 10 && number > 100) {
	    	montantFinale = Integer.toString(number).substring(1,3);
	    }
	   
	    if (number % 100 < 20){
	      montant = numNames[number % 100];
	      number /= 100;
	    }
	    else {
	      montant = numNames[number % 10];
	      number /= 10;

	      montant = tensNames[number % 10] + montant;
	      number /= 10;
	    }
	        
	    if (montantFinale.startsWith("7") || montantFinale.startsWith("9")) {
	    	
	    	montant = remplaceNombresFrancais(montantFinale);
	    }
	    
	    if (number == 0) return montant;
	    
	    if (number == 1) return "cent "+ montant;
	    
	    return numNames[number] + " cent " + montant;
	  }


	  public static String convertir(long number) {

	    if (number == 0) { return "zero"; }
	    
	    String snumber = Long.toString(number);

	    String formatNumber = "000000000000";
	    DecimalFormat df = new DecimalFormat(formatNumber);
	    snumber = df.format(number);


	    int milliards = Integer.parseInt(snumber.substring(0,3));

	    int millions  = Integer.parseInt(snumber.substring(3,6));

	    int Milliers = Integer.parseInt(snumber.substring(6,9));
	    
	    int centaines = Integer.parseInt(snumber.substring(9,12));

	    String resultMilliards;
	    switch (milliards) {
	    case 0:
	    	resultMilliards = "";
	      break;
	    case 1 :
	    	resultMilliards = convertirMoinsDeMille(milliards)
	      + " Milliard ";
	      break;
	    default :
	    	resultMilliards = convertirMoinsDeMille(milliards)
	      + " Milliards ";
	    }
	    String result =  resultMilliards;

	    String resultMillions;
	    switch (millions) {
	    case 0:
    	resultMillions = "";
	      break;
	    case 1 :
    	resultMillions = convertirMoinsDeMille(millions)
	         + " million ";
	      break;
	    default :
    	resultMillions = convertirMoinsDeMille(millions)
	         + " millions ";
	    }
	    result =  result + resultMillions;

	    String resultCentainesDeMilliers;
	    switch (Milliers) {
	    case 0:
    	resultCentainesDeMilliers = "";
	      break;
	    case 1 :
    	resultCentainesDeMilliers = "mille ";
	      break;
	    default :
    	resultCentainesDeMilliers = convertirMoinsDeMille(Milliers)
	         + " mille ";
	    }
	    result =  result + resultCentainesDeMilliers;

	    String resultCentaines;
	    resultCentaines = convertirMoinsDeMille(centaines);
	    result =  result + resultCentaines;


	    return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ") + " euros";
	  }
	  
	  public static String remplaceNombresFrancais(String montantFinale) {
		  
		  int dizaines = Integer.parseInt(montantFinale);
		  
		  if (montantFinale.substring(0,1).equalsIgnoreCase("7")) {
			  
			  HashMap<Integer, String> soixantaine = new HashMap<Integer, String>();
			  soixantaine.put(71, "soixante et onze");
			  soixantaine.put(72, "soixante douze");
			  soixantaine.put(73, "soixante treize");
			  soixantaine.put(74, "soixante quatorze");
			  soixantaine.put(75, "soixante quinze");
			  soixantaine.put(76, "soixante seize");
			  soixantaine.put(77, "soixante dix sept");
			  soixantaine.put(78, "soixante dix huit");
			  soixantaine.put(79, "soixante dix neuf");
			  
			  montantFinale = soixantaine.get(dizaines);
		  
		  }
		  if (montantFinale.substring(0,1).equalsIgnoreCase("9")) {
			  
			  HashMap<Integer, String> quatreVingtDizaine = new HashMap<Integer, String>();
			  quatreVingtDizaine.put(91, "quatre-vingt onze");
			  quatreVingtDizaine.put(92, "quatre-vingt douze");
			  quatreVingtDizaine.put(93, "quatre-vingt treize");
			  quatreVingtDizaine.put(94, "quatre-vingt quatorze");
			  quatreVingtDizaine.put(95, "quatre-vingt quinze");
			  quatreVingtDizaine.put(96, "quatre-vingt seize");
			  quatreVingtDizaine.put(97, "quatre-vingt-dix sept");
			  quatreVingtDizaine.put(98, "quatre-vingt-dix huit");
			  quatreVingtDizaine.put(99, "quatre-vingt-dix neuf");
			  
			  montantFinale = quatreVingtDizaine.get(dizaines);
			  
			  }
		  
		  return montantFinale;
	  }

	  /**
	   * testing
	   * @param args
	 * @throws IOException 
	   */
	  public static void main(String[] args) throws IOException {
		  java.util.Scanner entree =   new java.util.Scanner(System.in);
		  
		  boolean isActif = true;
		  		  
		  while (isActif) { 
				
			System.out.println("saisir un montant");			
			long montant = entree.nextLong();			
			System.out.println(convertir(montant));
		}
	  }
	}

